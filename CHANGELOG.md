# CHANGELOG

## 1.0.1 - 2024-10-03

### Fixed

- Compatibilité PHP 7.4
## 1.0.0 - 2024-09-25

### Added

- spip/filtres_images#4723 Première sortie, reprenant le code présent dans le plugins-dist `filtres_images`
