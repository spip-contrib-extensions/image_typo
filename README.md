# Image typographique

Ce projet contient le filtre `|image_typo` et les fonctions afférentes.

Ce filtre sera supprimé de SPIP avec la version 5.0.

A partir de la version 4.4 il est déprécié de SPIP. Il est possible d'installer ce plugin à la place.

[Documentation](https://www.spip.net/fr_article3325.html)
