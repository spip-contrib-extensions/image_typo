<?php

/**
 * Petit test pour gérer en douceur la transition
 * Permettre d'activer le plugin sur des sites où image_typo est encore fournit par SPIP
**/
if (!function_exists('image_typo')) {
	/**
	 * Créer une image typo
	 *
	 * @note
	 *    Cas particulier historique : son nom commence par "image_"
	 *    mais s'applique sur un texte…
	 * @return string
	 */
	function image_typo() {
		include_spip('inc/filtres_images_mini.php');
		include_spip('filtres/images_typo');
		$tous = func_get_args();
		return call_user_func_array('produire_image_typo', $tous);
	}
}
